odoo.define('pos_multi_pricelist.main', function (require) {
"use strict";
	
	var chrome = require('point_of_sale.chrome');
	var pos_model = require('point_of_sale.models');
	var core = require('web.core');
	var model_list = pos_model.PosModel.prototype.models;
	var screens = require('point_of_sale.screens');
	var _t = core._t;
	var Model = require('web.DataModel');
	var SuperOrder = pos_model.Order;
	var product_model = null;

	//--Fetching model dictionaries--
	for(var i = 0,len = model_list.length;i<len;i++){
		if(model_list[i].model == "product.product"){
			product_model = model_list[i];
			break;
		}
	}

	//--Updating model dictionaries--
	var super_product_loaded = product_model.loaded;
	product_model.loaded = function(self, products){
		self.all_products_ids = [];
		self.all_products = products;
		products.forEach(function(product) {
			self.all_products_ids.push(product.id);
		});
		super_product_loaded.call(this,self,products);
		$.blockUI({ message: '<h1 style="color: #c9d0d6;"><i class="fa fa-lock"></i> Screen Locked...</h1><center><p style="color: #f0f8ff;">Loading Pricelist Information..</p></center>' });
		new Model('product.product').call('get_price_by_pricelist', [{ 'product_ids': self.all_products_ids, 'currency_id': self.currency.id }])
		.then(function(price_dictionary) {
			self.db.price_dict = price_dictionary;
			$.unblockUI();
			if(self.chrome.screens)
				self.gui.pos.get_order().change_pricelist();	
		});
	};

	//--Loading Pricelists--
	pos_model.load_models({
		model: 'product.pricelist',
		fields: ['name', 'id', 'currency_id'],
		domain: function(self) {
			return [['currency_id', '=', self.currency.id]];
		},
		loaded: function(self, pricelists) {
			var pos_default_pricelist_id = self.config.pricelist_id[0];
			self.db.pricelist_by_id = {};
			pricelists.forEach(function(pricelist) {
				self.db.pricelist_by_id[pricelist.id] = pricelist;
				if (pricelist.id == pos_default_pricelist_id) {
					self.pricelist = pricelist;
					self.db.pos_default_pricelist = pricelist;
				}
			});
			self.pricelists = pricelists;
		},
	});

	screens.ProductScreenWidget.include({
		show: function(reset){
			var self = this;
			this._super(reset);
			if(self.chrome.screens != null && self.pos.db.price_dict != null)
				self.pos.get_order().change_pricelist();
		}
	});

	pos_model.Order = pos_model.Order.extend({
		initialize: function(attributes,options){
			self = this;
			SuperOrder.prototype.initialize.call(this,attributes,options);
			var pos_default_pricelist_id = self.pos.config.pricelist_id[0];
			if (options.json){
				if(options.json.pricelist_id == null){
					self.order_pricelist = self.pos.db.pricelist_by_id[pos_default_pricelist_id];
					self.pricelist = self.pos.db.pricelist_by_id[pos_default_pricelist_id];
				}
			}
			else{
				self.order_pricelist = self.pos.db.pricelist_by_id[pos_default_pricelist_id];
				self.pricelist = self.pos.db.pricelist_by_id[pos_default_pricelist_id];
			}
		},
		export_as_JSON: function() {
			var self = this;
			var loaded=SuperOrder.prototype.export_as_JSON.call(this);
			if(self.pos.get_order())
				loaded.pricelist_id=self.pos.get_order().order_pricelist.id;  
			return loaded;
		},
		init_from_JSON: function(json) {
			var self = this;
			SuperOrder.prototype.init_from_JSON.call(this,json);
			if(json.pricelist_id)
				self.order_pricelist = self.pos.db.pricelist_by_id[json.pricelist_id];				
		},
		change_pricelist: function() {
			self = this;
			var product_id;
			var product_price_for_pricelst;
			var formatted_price;
			var order_pricelist = self.order_pricelist;
			self.pos.pricelist = order_pricelist;
			self.pos.chrome.screens.products.product_categories_widget.reset_category();
			$('div#pricelist_select center p').text(order_pricelist.name);
			var all = $('.product');
			self.pos.all_products.forEach(function(product) {
				product.price = self.pos.db.price_dict[order_pricelist.id][product.id][0];
			});
			$.each(all, function(index, value) {
				product_id = $(value).data('product-id');
				product_price_for_pricelst = self.pos.db.price_dict[order_pricelist.id][product_id][0];
				formatted_price = self.pos.chrome.widget.order_selector.format_currency(product_price_for_pricelst);
				if (self.pos.db.product_by_id[product_id].to_weight)
					formatted_price += '/Kg';
				$(value).find('.price-tag').html(formatted_price);
			});
			self.pos.get_order().save_to_db();
		},
	});

	chrome.OrderSelectorWidget.include({
		renderElement: function(){
			var pricelist_id = ''
			var self = this;
			this._super();
			this.$('#pricelist_dropdown').hide();
			self.$('#pricelist_select').on("mouseover click",function(event)
			{
				if(self.gui.get_current_screen()=='products')
				{
					if(event.type=="click")
						$('#pricelist_dropdown').toggle();
					else
						$('#pricelist_dropdown').show();
				}
			});
			self.$('#pricelist_select').on("mouseout",function()
			{
				if(! $('#pricelist_dropdown').is(":hover"))
					$('#pricelist_dropdown').hide();	
			});

			self.$('li#pricelist_name').on("mouseover",function()
			{
				$(this).css('background-color','rgb(62, 36, 36)');
			});
			self.$('li#pricelist_name').on("mouseout",function()
			{
				$(this).css('background-color','');
				if(! $('#pricelist_dropdown').is(":hover") && ! $('#pricelist_select').is(":hover"))
					$('#pricelist_dropdown').hide();
			});
			self.$('li#pricelist_name').on('click',function(){
				var clicked_element = $(this);
				pricelist_id = clicked_element.attr('id_val');
				var all = $('.product');
				var user = self.pos.get_cashier();
				self.pos.get_order().order_pricelist = self.pos.db.pricelist_by_id[pricelist_id];
				if(parseInt(self.pos.get_order().orderlines.length)>0)
				{
					self.gui.show_popup('confirm',{
						'title': _t('Change Pricelist ?'),
						'body': _t('You already have some products added with the current pricelist.'),
						confirm: function(){
							if(user.pos_security_pin)
							{
								return self.gui.ask_password(user.pos_security_pin).then(function(){
									self.pos.get_order().change_pricelist();
								});
							}
							else
								self.pos.get_order().change_pricelist();
						},
					});
				}
				else
				{
					if(user.pos_security_pin)
					{
						return self.gui.ask_password(user.pos_security_pin).then(function(){
							self.pos.get_order().change_pricelist();
						});
					}
					else
						self.pos.get_order().change_pricelist();
				}
			});
		},
	});
});