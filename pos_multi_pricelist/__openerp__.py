# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "POS Multi Pricelist",
  "summary"              :  "Allows the seller to change the pricelist at runtime without having to reload the page.",
  "category"             :  "Point Of Sale",
  "version"              :  "1.5.1",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "website"              :  "https://store.webkul.com/Odoo-POS-Multi-Pricelist.html",
  "description"          :  "http://webkul.com/blog/pos-multi-pric/",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=pos_multi_pricelist&version=9.0",
  "depends"              :  [
                             'base',
                             'point_of_sale',
                             'product',
                            ],
  "demo"                 :  ['data/pos_multi_pricelist_demo.xml'],
  "data"                 :  ['views/template.xml'],
  "qweb"                 :  ['static/src/xml/pos_pricelist_view.xml'],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
#  "price"                :  69,
  "currency"             :  "EUR",
}
