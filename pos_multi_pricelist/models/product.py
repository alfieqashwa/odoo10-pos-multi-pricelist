# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#################################################################################
from openerp import api, fields,models
from openerp.exceptions import UserError, Warning, RedirectWarning
from openerp import SUPERUSER_ID

class PosOrder(models.Model):
	_inherit = 'pos.order'

	@api.model
	def _order_fields(self,ui_order):
		fields_return = super(PosOrder,self)._order_fields(ui_order)
		fields_return.update({'pricelist_id':ui_order.get('pricelist_id','')})
		return fields_return

class ProductProduct(models.Model):
	_inherit = "product.product"

	@api.model
	def get_price_by_pricelist(self,kwargs):
		result = {}
		for pricelist_obj in self.env['product.pricelist'].search([('currency_id','=',kwargs['currency_id'])]):
			values = dict()
			for product_id in kwargs.get('product_ids'):
				price = pricelist_obj.price_get(product_id,1.0, partner=None)[pricelist_obj.id]
				values[product_id] = [price]
			result[pricelist_obj.id] = values
		return result
	
	@api.model
	def enable_sale_pricelist_setting(self):
		sale_config_obj = self.env['sale.config.settings'].create({
			'sale_pricelist_setting':'formula'
		})
		sale_config_obj.update({
                'group_product_pricelist': False,
                'group_sale_pricelist': True,
				'group_pricelist_item': True,
			})
		sale_config_obj.execute()